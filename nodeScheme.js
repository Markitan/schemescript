let Horseman = require('node-horseman');
let horseman = new Horseman();
let fs = require('fs');
let prompt = require('prompt-sync')();
let Promise = require('bluebird');

let [, ...args] = process.argv;
let url = args[1];
let venue = prompt('Enter Venue name:');
let id = prompt('Enter Venue ID:');
let folderPath = '/Users/vasilijmarkitan/Desktop/SchemeProject/'+venue;
let links = [];

horseman
      .on('resourceRequested', (reqData) => {
        let link = reqData[0].url;
        if (~link.indexOf("req_number=")) {
          links.push(link);
        }
      })
      .open(url)
      .wait(4000)
      .cookies()
      .then((cookies) => {
            fs.mkdirSync(folderPath);
            console.log(`*****${venue} folder created*****`);
            return Promise.each(links, (link) => {
              return horseman
                .open(link)
                .plainText()
                .then((plainText) => {
                  if (~plainText.indexOf('{"session":{"id"')){
                    console.log('-----session JSON saved-----')
                    fs.writeFileSync(`${folderPath}/${id}-session.json`, plainText);
                  } else if (~plainText.indexOf('x_coord')) {
                    console.log('-----stage JSON saved-------')
                    fs.writeFileSync(`${folderPath}/${id}-stage.json`, plainText);
                  } else if (~plainText.indexOf('"result":"<svg xmlns')) {
                    console.log('-----svg JSON saved---------')
                    fs.writeFileSync(`${folderPath}/${id}-svg.json`, plainText);
                  } else if (~plainText.indexOf('"hallplan":{"totalSeatCount"')) {
                    console.log('-----hallplan JSON saved----')
                    fs.writeFileSync(`${folderPath}/${id}-hallplan.json`, plainText);
                  } else {return}
                  return Promise.resolve();
                })
            })

      })
      .close()
